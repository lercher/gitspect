package main

import (
	"encoding/json"
	"io"
	"log"
	"os"
	"path"
	"strconv"
	"strings"
	"time"

	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"gopkg.in/src-d/go-git.v4/plumbing"
)

// Stats records always increasing counts of files and dirs over a sequence of commits
type Stats struct {
	ID       string              `json:"id,omitempty"`
	Branch   string              `json:"branch,omitempty"`
	Origin   string              `json:"origin,omitempty"`
	LastHash string              `json:"lasthash,omitempty"`
	Commits  int                 `json:"commits,omitempty"`
	DirN     []map[string]Change `json:"dirn,omitempty"` // indexed by hierarchy, 0=root dir
	DirKind  []string            `json:"dirkind,omitempty"`
	File     map[string]Change   `json:"file,omitempty"` // indexed by path
}

// Change counts changes of a file or dir in a single commit
type Change struct {
	Touches int `json:"touches,omitempty"`
	Adds    int `json:"adds,omitempty"`
	Dels    int `json:"dels,omitempty"`
}

func newStats(id, branch, origin string) *Stats {
	return &Stats{
		ID:     id,
		Branch: branch,
		Origin: origin,
		File:   make(map[string]Change),
	}
}

func (s *Stats) add(sk *sink, who string, when time.Time, h plumbing.Hash, file string, additions, deletions int) {
	s.LastHash = h.String()
	
	ch := s.File[file]
	honorAdds := ch.touchAndAddAddsAndDels(additions, deletions)
	s.File[file] = ch
	s.writeTo(sk, who, when, h, file, ch, "file")

	if honorAdds {
		s.addpaths(sk, who, when, h, file, additions, deletions)
	} else {
		s.addpaths(sk, who, when, h, file, 0, 0)
	}
}

func (s *Stats) addpaths(sk *sink, who string, when time.Time, h plumbing.Hash, file string, additions, deletions int) {
	var p string

	level := 0
	p, _ = path.Split(file)
	p = strings.TrimSuffix(p, "/")
	for {
		level++
		if level > len(s.DirN) {
			s.DirN = append(s.DirN, map[string]Change{})
			s.DirKind = append(s.DirKind, "dir"+strconv.Itoa(level-1))
		}
		if p == "" {
			break
		}
		p, _ = path.Split(p)
		p = strings.TrimSuffix(p, "/")
	}

	p, _ = path.Split(file)
	p = strings.TrimSuffix(p, "/")
	for {
		level--
		ch := s.DirN[level][p]
		ch.touchAndAddAddsAndDels(additions, deletions)
		s.DirN[level][p] = ch
		s.writeTo(sk, who, when, h, p, ch, s.DirKind[level])

		if p == "" {
			break
		}
		p, _ = path.Split(p)
		p = strings.TrimSuffix(p, "/")
	}
}

func (ch *Change) touchAndAddAddsAndDels(additions, deletions int) bool {
	ch.Touches++
	if ch.Touches > 1 {
		// we record additions only if it was not the first touch of the file
		// therefor big files are ignored if they are not changed after importing
		ch.Adds += additions
		ch.Dels += deletions
		return true
	}
	return false
}

func (s Stats) writeTo(sk *sink, who string, when time.Time, h plumbing.Hash, what string, ch Change, kind string) {
	if sk == nil {
		return
	}

	ext := "(dir)"
	name := what

	switch kind {
	case "file":
		ext = strings.TrimPrefix(path.Ext(what), ".")
		if ext == "" {
			ext = "(none)"
		}
		_, name = path.Split(what)

	default:
		if what == "" {
			name = "/"
		}
	}

	p := influxdb2.NewPointWithMeasurement("git").
		AddTag("author", who).
		AddTag("branch", s.Branch).
		AddTag("ext", ext).
		AddTag("full", what).
		AddTag("kind", kind).
		AddTag("name", name).
		AddTag("repo", s.ID).
		AddField("add", ch.Adds).
		AddField("count", ch.Touches).
		AddField("del", ch.Dels).
		//AddField("hash", h.String()).
		SetTime(when)
	sk.writeAPI.WritePoint(p) // write point asynchronously

	// log.Println(when.Format(time.RFC3339), kind, ch.touches, who, what)
}

func (s *Stats) fileName() string {
	return s.ID + "-" + s.Branch + ".json"
}

func (s *Stats) loadFromFile() (string, error) {
	fn := s.fileName()
	f, err := os.Open(fn)
	if err != nil {
		log.Println(err)
		return fn, nil
	}
	defer f.Close()

	return fn, s.deserialize(f)
}

func (s *Stats) deserialize(r io.Reader) error {
	rd := json.NewDecoder(r)
	return rd.Decode(s)
}

func (s *Stats) serialize(w io.Writer) error {
	wr := json.NewEncoder(w)
	wr.SetIndent("", "  ")
	return wr.Encode(s)
}

func (s *Stats) serializeToFile() (string, error) {
	fn := s.fileName()
	f, err := os.Create(fn)
	if err != nil {
		return fn, err
	}
	defer f.Close()

	return fn, s.serialize(f)
}
