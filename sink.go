package main

import (
	influxdb2 "github.com/influxdata/influxdb-client-go/v2"
	"github.com/influxdata/influxdb-client-go/v2/api"
)


type sink struct {
	url      string
	token    string
	bucket   string
	org      string
	client   influxdb2.Client
	writeAPI api.WriteAPI
}

func (sk *sink) connect() {
	sk.close()
	sk.client = influxdb2.NewClient(sk.url, sk.token)
	sk.writeAPI = sk.client.WriteAPI(sk.org, sk.bucket)
}

func (sk *sink) close() {
	if sk != nil && sk.client != nil {
		sk.writeAPI = nil
		sk.client.Close()
		sk.client = nil
	}
}
