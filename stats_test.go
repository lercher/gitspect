package main

import (
	"fmt"
	"strings"
	"testing"
	"time"

	"gopkg.in/src-d/go-git.v4/plumbing"
)

func Test_stats_add(t *testing.T) {
	s := &Stats{
		File: make(map[string]Change),
	}

	const (
		f0       = "root.txt"
		f1       = "templates/readme.md"
		f2foo    = "templates/x/foo.txt"
		f2bar    = "templates/x/bar.txt"
		f2foobar = "templates/foo/bar.html"
	)
	s.add(nil, "", time.Time{}, plumbing.Hash{}, f0, 10000, 20000)
	if s.DirN[0][""].Touches != 1 || s.DirN[0][""].Adds != 0 || s.DirN[0][""].Dels != 0 {
		t.Errorf("want t,a,d = 1,0,0, got %v,%v,%v", s.DirN[0][""].Touches, s.DirN[0][""].Adds, s.DirN[0][""].Dels)
	}

	s.add(nil, "", time.Time{}, plumbing.Hash{}, f0, 10000, 0)
	if s.DirN[0][""].Touches != 2 || s.DirN[0][""].Adds != 10000 || s.DirN[0][""].Dels != 0 {
		t.Errorf("want t,a,d = 2,10000,0, got %v,%v,%v", s.DirN[0][""].Touches, s.DirN[0][""].Adds, s.DirN[0][""].Dels)
	}

	s.add(nil, "", time.Time{}, plumbing.Hash{}, f0, 0, 20000)
	if s.DirN[0][""].Touches != 3 || s.DirN[0][""].Adds != 10000 || s.DirN[0][""].Dels != 20000 {
		t.Errorf("want t,a,d = 3,10000,20000, got %v,%v,%v", s.DirN[0][""].Touches, s.DirN[0][""].Adds, s.DirN[0][""].Dels)
	}

	s.add(nil, "", time.Time{}, plumbing.Hash{}, f1, 1000, 2000)

	s.add(nil, "", time.Time{}, plumbing.Hash{}, f2foo, 5, 7)

	s.add(nil, "", time.Time{}, plumbing.Hash{}, f2bar, 77, 88)
	s.add(nil, "", time.Time{}, plumbing.Hash{}, f2bar, 10, 20)

	s.add(nil, "", time.Time{}, plumbing.Hash{}, f2foobar, 100, 200)

	t.Logf("dirKind: %#v", s.DirKind)
	t.Logf("dir N=%#v", len(s.DirN))
	t.Logf("dir0: %#v", s.DirN[0])
	t.Logf("dir1: %#v", s.DirN[1])
	t.Logf("dir2: %#v", s.DirN[2])
	t.Logf("file: %-25v %#v", f0, s.File[f0])
	t.Logf("file: %-25v %#v", f1, s.File[f1])
	t.Logf("file: %-25v %#v", f2foo, s.File[f2foo])
	t.Logf("file: %-25v %#v", f2bar, s.File[f2bar])
	t.Logf("file: %-25v %#v", f2foobar, s.File[f2foobar])

	if got, want := fmt.Sprint(s.DirKind), "[dir0 dir1 dir2]"; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := len(s.DirN), len(s.DirKind); got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := s.DirN[0][""].Touches, 8; got != want {
		t.Errorf("want %v, got %v", want, got)
	}
	if got, want := s.DirN[0][""].Adds, 10010; got != want {
		t.Errorf("want %v, got %v", want, got)
	}
	if got, want := s.DirN[0][""].Dels, 20020; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := s.DirN[1]["templates"].Touches, 5; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := s.DirN[2]["templates/x"].Touches, 3; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	if got, want := s.DirN[2]["templates/foo"].Touches, 1; got != want {
		t.Errorf("want %v, got %v", want, got)
	}

	for n, dir := range s.DirN {
		for k := range dir {
			if strings.HasSuffix(k, "/") {
				t.Errorf("dirN[%v] has trailing / directory %v", n, k)
			}
		}
	}
}
