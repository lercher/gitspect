package main

import (
	"flag"
	"fmt"
	"log"
	"path"
	"strings"
	"time"

	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
	"gopkg.in/src-d/go-git.v4/plumbing/storer"
)

var (
	flagBranch = flag.String("r", "", "path of the repo to inspect, if empty use current directory")
	flagFull   = flag.Bool("f", false, "do a full export, even if a preserved stats file repoID-branch.json exists")
)

func main() {
	log.Println("This is gitspect. (C) 2021 by Martin Lercher, but free to use and modify")
	log.Println("as long as you respect my intellectual property by preserving my name")
	log.Println("and a reference to the original sources at https://gitlab.com/lercher/gitspect")
	log.Println("")
	log.Println("gitspect inspects a given git repository and sends statistical data")
	log.Println("over commits and changes of files and directories to an InfluxDB server")
	log.Println("for analysis. See the flux subfolder for some sample flux queries.")
	log.Println("After each run, the latest statistics are preserved in the file repoID-branch.json")
	log.Println("and on any later call only commits after these stats are inspected and added to")
	log.Println("the preserved ones")
	log.Println("")
	log.Println("note: the repo must have at least one remote called origin, b/c we derive")
	log.Println("a repoID from the last path element of this git-remote-url. E.g. for this repo")
	log.Println("git remote -v -> origin  git@gitlab.com:lercher/gitspect.git (fetch)")
	log.Println("we use 'gitpspect' as the repoID")
	log.Println("")

	sk := &sink{}
	flag.StringVar(&sk.url, "s", "http://localhost:8086", "URL of the InfluxDB server where the stats are sent to")
	flag.StringVar(&sk.bucket, "b", "munich", "name of the InfluxDB BUCKET")
	flag.StringVar(&sk.org, "o", "personal", "name of the InfluxDB ORGANIZATION")
	flag.StringVar(&sk.token, "t", "s9WItYfufX11Gcqjq-BiNQNK2MwquCoTQyq7OEQr4XHXkLMHZ32K_61f2TEdgcX14oy3Nhi1ive1ieVHuh5UMg==", "TOKEN for InfluxDB auth")

	flag.Parse()
	// log.SetFlags(log.Lshortfile | log.Flags())

	repo := "."
	if *flagBranch != "" {
		repo = *flagBranch
	}

	r, err := git.PlainOpen(repo)
	if err != nil {
		log.Fatalf("%v: %v", repo, err)
	}

	head, err := r.Head()
	if err != nil {
		log.Fatalf("%v HEAD: %v", repo, err)
	}

	log.Println("HEAD", head)
	if head.Name().IsBranch() {
		log.Println("current branch:", head.Name().Short())
	} else {
		log.Println("HEAD is not on a branch")
	}

	remotes, err := r.Remotes()
	if err != nil {
		log.Fatalf("remotes %v: %v", repo, err)
	}

	origin := "(unknown)"
	for _, rm := range remotes {
		if rm.Config().Name == "origin" && len(rm.Config().URLs) > 0 {
			origin = rm.Config().URLs[0]
			break
		}
	}
	log.Println("origin:", origin)

	_, fn := path.Split(origin)
	id := strings.TrimSuffix(fn, path.Ext(fn))

	log.Println("using id ->", id, "<- for further references (file base name of the origin)")

	err = inspect(sk, r, id, head.Name().Short(), origin, *flagFull)
	if err != nil {
		log.Fatalf("inspect: %v", err)
	}
	log.Println("DONE")
}

func inspect(sk *sink, r *git.Repository, id, branch, origin string, full bool) error {
	s := newStats(id, branch, origin)

	if !full {
		fn, err := s.loadFromFile()
		if err != nil {
			return fmt.Errorf("loading stats %v: %v", fn, err)
		}
		if s.LastHash != "" {
			log.Println("last inspected hash is", s.LastHash, "with", s.Commits, "commits. Looking for newer commits ...")
		}
	}
	log.Println("inspecting", id, branch)

	iter, err := r.Log(&git.LogOptions{
		Order: git.LogOrderCommitterTime,
	})
	if err != nil {
		return fmt.Errorf("log: %v", err)
	}

	var list []plumbing.Hash
	var lastOneHasParents bool
	err = iter.ForEach(func(c *object.Commit) error {
		if s.LastHash == c.ID().String() {
			return storer.ErrStop
		}

		log.Println(c.ID(), c.Author.When.Format(time.RFC3339), c.Author.Name)
		np := len(c.ParentHashes)
		lastOneHasParents = (np >= 1)
		hasMultipleParents := (np >= 2)

		if !hasMultipleParents {
			// only consider if it has 1 or 0 parents, i.e. it is no merge commit
			list = append(list, c.Hash)
		}

		return nil
	})
	if err != nil && err.Error() == "object not found" && lastOneHasParents {
		// most probably a shallow copy of a repo, i.e. we don't have more history
		log.Println("this was most probably a shallow copy of a repo, i.e. we don't have more history than that. Processing what we have.")
		err = nil
	}
	if err != nil {
		return fmt.Errorf("iterate log #%v git fsck/git gc might help, see also 'git log %v': %v", len(list), list[len(list)-1], err)
	}

	listLen := len(list)
	log.Println("found", listLen, "commits")

	sk.connect()
	defer sk.close()

	for i := range list {
		idx := listLen - 1 - i
		err = inspectByref(sk, s, r, list[idx])
		if err != nil {
			return fmt.Errorf("inspecting ref %v of %v ref %v: %v", idx, listLen, list[idx], err)
		}
	}
	log.Println("transferred", listLen, "commits. Serializing stats ...")
	sk.close()

	fn, err := s.serializeToFile()
	if err != nil {
		return fmt.Errorf("serializing stats: %v", err)
	}
	log.Println("serialized stats to", fn)

	return nil
}

func inspectByref(sk *sink, s *Stats, r *git.Repository, h plumbing.Hash) error {
	commit, err := r.CommitObject(h)
	if err != nil {
		return fmt.Errorf("commit by ref %v: %v", h, err)
	}
	stats, err := commit.Stats()
	if err != nil {
		return fmt.Errorf("file stats by ref %v: %v", h, err)
	}

	for _, st := range stats {
		s.add(sk, commit.Author.Name, commit.Author.When, h, st.Name, st.Addition, st.Deletion)
	}
	s.Commits++

	log.Println(h, commit.Author.When, commit.Author.Name)
	return nil
}
