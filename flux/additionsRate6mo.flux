delta = (tables=<-) => 
tables
    |> sort(columns: ["_time"])
    |> reduce(
        identity: {fst: 0, sum: 0, cnt: 0},
        fn: (r, accumulator) => (if accumulator.cnt == 0 then {
                fst: r._value,
                cnt: 1,
                sum: 0,
            } else {
                fst: accumulator.fst,
                cnt: accumulator.cnt + 1,
                sum: r._value - accumulator.fst
            }
        )
    )
    |> drop(columns: ["fst", "cnt"])
    |> rename(columns: {sum: "_value"})

from(bucket: "munich")
  |> range(start: -6mo)
  |> filter(fn: (r) => r._measurement == "git" and r._field == "add" and r.repo == v.gitRepo and r.kind == "file")
  |> keep(columns: ["full", "name", "_value", "_time"])
  |> window(every: 1d, period: 14d)
  |> delta()
  |> duplicate(column: "_stop", as: "_time")
  |> window(every: inf)
  |> yield(name: "last")